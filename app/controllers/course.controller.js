//Import thư viện mongoose
const mongoose = require('mongoose');

//Import model
const courseModel = require('../models/course.model')

//get all courses
const getAllCourses = async (req, res) => {
    console.log('Get All Courses');
    try {
        //B1: thu thập dữ liệu (bỏ qua)
        let courseName = req.query.name;
        let minStudent = req.query.min;
        let maxStudent = req.query.max;
        
        let condition = {}

        if (courseName) {
            condition.title = {$regex:courseName, '$options' : 'i'}
        }

        if (minStudent) {
            condition.noStudent = {$gte:minStudent}
        }

        if (maxStudent) {
            condition.noStudent = { ...condition.noStudent, $lte:maxStudent}
        }
        
/*
        let condition = {
            title:{$regex:courseName, '$options' : 'i'},
            $or:[
                {noStudent:{$lte:minStudent}},
                {noStudent:{$gte:maxStudent}}
            ]
        }
*/
        /*
        let condition = {
            title:courseName,
            noStudent:{
                $gte:minStudent,
                $lte:maxStudent
            }
        }*/

        console.log(condition);
        //B2: kiểm tra dữ liệu (bỏ qua)
        //B3: thao tác CSDL
        const result = await courseModel.find(condition)
        .sort({title:'desc'})
        .skip(1)
        .populate("reviews");
        console.log(result);
        res.status(200).json({
            status:"Get all courses successfully!",
            data:result
        })
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//get a course by id
const getCourseById = async (req, res) => {
    console.log('Get course by id');
    try {
        //B1: thu thập dữ liệu
        const courseid = req.params.courseid;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(courseid)) {
            return res.status(400).json({
                stutus:"Bad request",
                message:"CourseId is invalid!"
            })
        }

        //B3: thao tác CSDL
        const result = await courseModel.findById(courseid).populate("reviews");
        console.log(result);
        res.status(200).json({
            status:"Get all courses successfully!",
            data:result
        })
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


//create new course
const createNewCourse = async (req, res) => {
    console.log('Create a course');
    try {
        //B1: thu thập dữ liệu
        console.log(req.body);
        const { title, description, noStudent} = req.body;

        //B2: kiểm tra dữ liệu
        if (!title || title == undefined){
            return res.status(400).json({
                stutus:"Bad request",
                message:"Title is required!"
            })
        }

        if (!Number.isInteger(noStudent) || noStudent <=0) {
            return res.status(400).json({
                stutus:"Bad request",
                message:"NoStudent is invalid!"
            })
        }

        //B3: thao tác CSDL
        let newCourse = {
            _id:new mongoose.Types.ObjectId(),
            title,
            description,
            noStudent
        }
        
        const result = await courseModel.create(newCourse);
        return res.status(201).json({
            status:"Create new course successfully!",
            data:result
        })
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//update a course by id
const updateCourseById = async (req, res) => {
    console.log('Update course by id');
    try {
        //B1: thu thập dữ liệu
        const courseid = req.params.courseid;
        const { title, description, noStudent} = req.body;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(courseid)) {
            return res.status(400).json({
                stutus:"Bad request",
                message:"CourseId is invalid!"
            })
        }

        if (!title || title == undefined){
            return res.status(400).json({
                stutus:"Bad request",
                message:"Title is required!"
            })
        }

        if (!Number.isInteger(noStudent) || noStudent <=0) {
            return res.status(400).json({
                stutus:"Bad request",
                message:"NoStudent is invalid!"
            })
        }

        //B3: thao tác CSDL
        let updateCourse = {}
        if (title != undefined) {
            updateCourse.title = title;
        }
        if (description != undefined) {
            updateCourse.description = description;
        }
        if (noStudent != undefined) {
            updateCourse.noStudent = noStudent;
        }

        const result = await courseModel.findByIdAndUpdate(courseid, updateCourse);
        return res.status(200).json({
            status:"Update a course successfully!",
            data:result
        })
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//delete a course by id
const deleteCourseById = async (req, res) => {
    console.log('Delete course by id');
    try {
        //B1: thu thập dữ liệu
        const courseid = req.params.courseid;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(courseid)) {
            return res.status(400).json({
                stutus:"Bad request",
                message:"CourseId is invalid!"
            })
        }

        //B3: thao tác
        const result = await courseModel.findByIdAndDelete(courseid);
        return res.status(204).json({
            status:"Delete a course successfully!",
            data:result
        })
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllCourses,
    getCourseById,
    createNewCourse,
    updateCourseById,
    deleteCourseById
}