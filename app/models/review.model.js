// B1: Khai báo thư viện mongoose
const mongoose = require('mongoose');

// B2: Khai báo Schema
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema theo thông tin cần quản lý
const reviewSchema = new Schema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
} , {
    // Trường thông tin thời gian, dùng để lưu dấu document (lịch sử)
    timestamps: true
});

// B4: Biên dịch thành model và xuất ra module
module.exports = mongoose.model("review", reviewSchema);