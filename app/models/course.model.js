// B1: Khai báo thư viện mongoose
const mongoose = require('mongoose');

// B2: Khai báo Schema
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema theo thông tin cần quản lý
const courseSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    title: {
        type: String, 
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "review"
        }
    ]
} , {
    // Trường thông tin thời gian, dùng để lưu dấu document (lịch sử)
    timestamps: true
});

// B4: Biên dịch thành model và xuất ra module
module.exports = mongoose.model("course", courseSchema);