//khai báo thư viện express
const express = require('express')

//khai báo router
const router = express.Router();

//import middleware
const reviewMiddleware = require('../middlewares/review.middleware')

//import controller
const reviewController = require('../controllers/review.controller')

router.use(reviewMiddleware.ReviewCommon);

//khai báo các request
//get all review
router.get('/', reviewMiddleware.GetAllReview, reviewController.getAllReviews)

//get a review by id
router.get('/:reviewid', reviewMiddleware.GetReviewByID, reviewController.getReviewById)

//create new review
router.post('/', reviewMiddleware.CreateReview, reviewController.createNewView)

//update review by id
router.put('/:reviewid', reviewMiddleware.UpdateReviewByID, reviewController.updateReviewById)

//delete review by id
router.delete('/:reviewid/courses/:courseid', reviewMiddleware.DeleteReviewByID, reviewController.deleteReviewById)

module.exports = router;