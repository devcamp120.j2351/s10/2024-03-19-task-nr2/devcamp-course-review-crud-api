//Import thư viện mongoose
const mongoose = require('mongoose');

//Import model
const courseModel = require('../models/course.model')
const reviewModel = require("../models/review.model")

//get all reviews
const getAllReviews = async (req, res) => {
    console.log('Get All Reviews');
    try {
        //B1: thu thập dữ liệu (bỏ qua)
        //B2: kiểm tra dữ liệu (bỏ qua)
        //B3: thao tác CSDL
        const result = await reviewModel.find()
        return res.status(200).json({
            status:"Load all reviews successfully!",
            data:result
        })
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//get review by id
const getReviewById = async (req, res) => {
    console.log('Get review by id');

    try {
        //B1: thu thập dữ liệu
        const reviewid = req.params.reviewid;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(reviewid)) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Review Id is invalid!"
            })
        }

        //B3: thao tác CSDL
        const result = await reviewModel.findById(reviewid)
        if (result) {
            return res.status(200).json({
                status:"Load review by id successfully!",
                data:result
            })
        } else {
            return res.status(404).json({
                status:"Review Not found!"
            })
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//create new review
const createNewView = async (req, res) => {
    console.log('Create a review');
    try {
        //B1: thu thập dữ liệu
        console.log(req.body);
        const { stars, note, courseId } = req.body;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Course Id is invalid!"
            })
        }
    
        if (isNaN(stars) || stars > 5 || stars < 0) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Stars is invalid!"
            })
        }
    
        if (!note) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Note is required!"
            })
        }

        //B3: thao tác CSDL
        const newReview = {
            _id:new mongoose.Types.ObjectId(),
            stars,
            note
        }
        //tạo mơi review
        const createdReview = await reviewModel.create(newReview);

        //thêm reviewid vào array reviews of course
        const savedCourse = await courseModel.findByIdAndUpdate(courseId, {
            $push: { reviews : createdReview._id}
        })

        if (createdReview) {
            return res.status(200).json({
                status:"Created new review successfully!",
                data:createdReview
            })
        } else {
            return res.status(500).json({
                status:"cannot create new review!",
            })
        }

    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//update review by id
const updateReviewById = async (req, res) => {
    console.log('Update review by id');
    try {
        //B1: thu thập dữ liệu
        const reviewid = req.params.reviewid;
        const { stars, note } = req.body;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(reviewid)) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Review Id is invalid!"
            })
        }
        if (isNaN(stars) || stars > 5 || stars < 0) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Stars is invalid!"
            })
        }

        if (!note) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Note is required!"
            })
        }

        //B3: thao tác CSDL
        const updateReview = {}
        if (stars) {
            updateReview.stars = stars;
        }

        if (note) {
            updateReview.note = note;
        }

        const udpatedReview = await reviewModel.findByIdAndUpdate(reviewid, updateReview)
        if (udpatedReview) {
            return res.status(200).json({
                status:"Update review by id successfully!",
                data:udpatedReview
            })
        } else {
            return res.status(404).json({
                status:"Review Not found!"
            })
        }

    } catch (error) {
        console.error(error);
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

//delete review by id
const deleteReviewById = async (req, res) => {
    console.log('Delete review by id');
    try {
        //B1: thu thập dữ liệu
        const courseid = req.params.courseid;
        const reviewid = req.params.reviewid;

        //B2: kiểm tra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(courseid)) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Course Id is invalid!"
            })
        }
        if (!mongoose.Types.ObjectId.isValid(reviewid)) {
            return res.status(404).json({
                status:"Bad request!",
                message:"Review Id is invalid!"
            })
        }

        //xóa review
        const deletedReview = await reviewModel.findByIdAndDelete(reviewid);

        //xóa reviewid trong course
        const deletedCourse = await courseModel.findByIdAndUpdate(courseid, {
            $pull : { reviews : reviewid}
        })
        if (deletedReview) {
            return res.status(200).json({
                status:"Delete review by id successfully!",
                data:deletedReview
            })
        } else {
            return res.status(404).json({
                status:"Review Not found!"
            })
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllReviews,
    getReviewById,
    createNewView,    
    updateReviewById,
    deleteReviewById
}